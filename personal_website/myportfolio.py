from flask import Flask, render_template
app = Flask(__name__)


@app.route("/")
def home():
    return render_template('home.html')

@app.route("/researchprocess")
def researchprocess():
    return render_template('researchprocess.html')

@app.route("/casestudies")
def casestudies ():
    return render_template('casestudies.html')

@app.route("/gitlabnav")
def nav ():
    return render_template('gitlabnav.html')

if __name__ == '__main__':
    app.run(debug=True)
